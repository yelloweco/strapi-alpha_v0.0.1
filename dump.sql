/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `remark` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_CATEGORIES` (`name`,`remark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `context` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_CONTACTS` (`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table core_store
# ------------------------------------------------------------

DROP TABLE IF EXISTS `core_store`;

CREATE TABLE `core_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `environment` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_CORE_STORE` (`key`,`value`,`type`,`environment`,`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `core_store` WRITE;
/*!40000 ALTER TABLE `core_store` DISABLE KEYS */;

INSERT INTO `core_store` (`id`, `key`, `value`, `type`, `environment`, `tag`)
VALUES
	(1,'db_model_core_store','{\"key\":{\"type\":\"string\"},\"value\":{\"type\":\"text\"},\"type\":{\"type\":\"string\"},\"environment\":{\"type\":\"string\"},\"tag\":{\"type\":\"string\"}}','object',NULL,NULL),
	(2,'db_model_users-permissions_permission','{\"type\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"controller\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"action\":{\"type\":\"string\",\"required\":true,\"configurable\":false},\"enabled\":{\"type\":\"boolean\",\"required\":true,\"configurable\":false},\"policy\":{\"type\":\"string\",\"configurable\":false},\"role\":{\"model\":\"role\",\"via\":\"permissions\",\"plugin\":\"users-permissions\",\"configurable\":false}}','object',NULL,NULL),
	(3,'db_model_upload_file','{\"name\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"hash\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"sha256\":{\"type\":\"string\",\"configurable\":false},\"ext\":{\"type\":\"string\",\"configurable\":false},\"mime\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"size\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"url\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"provider\":{\"type\":\"string\",\"configurable\":false,\"required\":true},\"provider_metadata\":{\"type\":\"json\",\"configurable\":false},\"related\":{\"collection\":\"*\",\"filter\":\"field\",\"configurable\":false},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(4,'db_model_strapi_administrator','{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"private\":true,\"required\":true},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false}}','object',NULL,NULL),
	(5,'db_model_users-permissions_role','{\"name\":{\"type\":\"string\",\"minLength\":3,\"required\":true,\"configurable\":false},\"description\":{\"type\":\"string\",\"configurable\":false},\"type\":{\"type\":\"string\",\"unique\":true,\"configurable\":false},\"permissions\":{\"collection\":\"permission\",\"via\":\"role\",\"plugin\":\"users-permissions\",\"configurable\":false,\"isVirtual\":true},\"users\":{\"collection\":\"user\",\"via\":\"role\",\"configurable\":false,\"plugin\":\"users-permissions\",\"isVirtual\":true}}','object',NULL,NULL),
	(6,'db_model_users-permissions_user','{\"username\":{\"type\":\"string\",\"minLength\":3,\"unique\":true,\"configurable\":false,\"required\":true},\"email\":{\"type\":\"email\",\"minLength\":6,\"configurable\":false,\"required\":true},\"provider\":{\"type\":\"string\",\"configurable\":false},\"password\":{\"type\":\"password\",\"minLength\":6,\"configurable\":false,\"private\":true},\"resetPasswordToken\":{\"type\":\"string\",\"configurable\":false,\"private\":true},\"confirmed\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"blocked\":{\"type\":\"boolean\",\"default\":false,\"configurable\":false},\"role\":{\"model\":\"role\",\"via\":\"users\",\"plugin\":\"users-permissions\",\"configurable\":false},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(7,'db_model_upload_file_morph','{\"upload_file_id\":{\"type\":\"integer\"},\"related_id\":{\"type\":\"integer\"},\"related_type\":{\"type\":\"text\"},\"field\":{\"type\":\"text\"}}','object',NULL,NULL),
	(8,'plugin_email_provider','{\"provider\":\"sendmail\",\"name\":\"Sendmail\",\"auth\":{\"sendmail_default_from\":{\"label\":\"Sendmail Default From\",\"type\":\"text\"},\"sendmail_default_replyto\":{\"label\":\"Sendmail Default Reply-To\",\"type\":\"text\"}}}','object','development',''),
	(9,'plugin_upload_provider','{\"provider\":\"local\",\"name\":\"Local server\",\"enabled\":true,\"sizeLimit\":1000000}','object','development',''),
	(10,'plugin_users-permissions_grant','{\"email\":{\"enabled\":true,\"icon\":\"envelope\"},\"discord\":{\"enabled\":false,\"icon\":\"comments\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/discord/callback\",\"scope\":[\"identify\",\"email\"]},\"facebook\":{\"enabled\":false,\"icon\":\"facebook-official\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/facebook/callback\",\"scope\":[\"email\"]},\"google\":{\"enabled\":false,\"icon\":\"google\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/google/callback\",\"scope\":[\"email\"]},\"github\":{\"enabled\":false,\"icon\":\"github\",\"key\":\"\",\"secret\":\"\",\"redirect_uri\":\"/auth/github/callback\",\"scope\":[\"user\",\"user:email\"]},\"microsoft\":{\"enabled\":false,\"icon\":\"windows\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/microsoft/callback\",\"scope\":[\"user.read\"]},\"twitter\":{\"enabled\":false,\"icon\":\"twitter\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/twitter/callback\"},\"instagram\":{\"enabled\":false,\"icon\":\"instagram\",\"key\":\"\",\"secret\":\"\",\"callback\":\"/auth/instagram/callback\"}}','object','',''),
	(11,'plugin_content_manager_configuration_content_types::admin.administrator','{\"uid\":\"administrator\",\"source\":\"admin\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"Username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"Email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Email\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"Password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"ResetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"ResetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"Blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Blocked\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"resetPasswordToken\"],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"password\",\"size\":6},{\"name\":\"resetPasswordToken\",\"size\":6}],[{\"name\":\"blocked\",\"size\":4}]],\"editRelations\":[]}}','object','',''),
	(12,'plugin_users-permissions_email','{\"reset_password\":{\"display\":\"Email.template.reset_password\",\"icon\":\"refresh\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"­Reset password\",\"message\":\"<p>We heard that you lost your password. Sorry about that!</p>\\n\\n<p>But don’t worry! You can use the following link to reset your password:</p>\\n\\n<p><%= URL %>?code=<%= TOKEN %></p>\\n\\n<p>Thanks.</p>\"}},\"email_confirmation\":{\"display\":\"Email.template.email_confirmation\",\"icon\":\"check-square-o\",\"options\":{\"from\":{\"name\":\"Administration Panel\",\"email\":\"no-reply@strapi.io\"},\"response_email\":\"\",\"object\":\"Account confirmation\",\"message\":\"<p>Thank you for registering!</p>\\n\\n<p>You have to confirm your email address. Please click on the link below.</p>\\n\\n<p><%= URL %>?confirmation=<%= CODE %></p>\\n\\n<p>Thanks.</p>\"}}}','object','',''),
	(13,'plugin_content_manager_configuration_content_types::users-permissions.permission','{\"uid\":\"permission\",\"source\":\"users-permissions\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"type\",\"defaultSortBy\":\"type\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"controller\":{\"edit\":{\"label\":\"Controller\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Controller\",\"searchable\":true,\"sortable\":true}},\"action\":{\"edit\":{\"label\":\"Action\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Action\",\"searchable\":true,\"sortable\":true}},\"enabled\":{\"edit\":{\"label\":\"Enabled\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Enabled\",\"searchable\":true,\"sortable\":true}},\"policy\":{\"edit\":{\"label\":\"Policy\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Policy\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"Role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Role\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"type\",\"controller\",\"action\"],\"edit\":[[{\"name\":\"type\",\"size\":6},{\"name\":\"controller\",\"size\":6}],[{\"name\":\"action\",\"size\":6},{\"name\":\"enabled\",\"size\":4}],[{\"name\":\"policy\",\"size\":6}]],\"editRelations\":[\"role\"]}}','object','',''),
	(14,'plugin_content_manager_configuration_content_types::users-permissions.role','{\"uid\":\"role\",\"source\":\"users-permissions\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"description\":{\"edit\":{\"label\":\"Description\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Description\",\"searchable\":true,\"sortable\":true}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"permissions\":{\"edit\":{\"label\":\"Permissions\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"type\"},\"list\":{\"label\":\"Permissions\",\"searchable\":false,\"sortable\":false}},\"users\":{\"edit\":{\"label\":\"Users\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"username\"},\"list\":{\"label\":\"Users\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"name\",\"description\",\"type\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"description\",\"size\":6}],[{\"name\":\"type\",\"size\":6}]],\"editRelations\":[\"permissions\",\"users\"]}}','object','',''),
	(15,'plugin_content_manager_configuration_content_types::upload.file','{\"uid\":\"file\",\"source\":\"upload\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"hash\":{\"edit\":{\"label\":\"Hash\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Hash\",\"searchable\":true,\"sortable\":true}},\"sha256\":{\"edit\":{\"label\":\"Sha256\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Sha256\",\"searchable\":true,\"sortable\":true}},\"ext\":{\"edit\":{\"label\":\"Ext\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Ext\",\"searchable\":true,\"sortable\":true}},\"mime\":{\"edit\":{\"label\":\"Mime\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Mime\",\"searchable\":true,\"sortable\":true}},\"size\":{\"edit\":{\"label\":\"Size\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Size\",\"searchable\":true,\"sortable\":true}},\"url\":{\"edit\":{\"label\":\"Url\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Url\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"Provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Provider\",\"searchable\":true,\"sortable\":true}},\"provider_metadata\":{\"edit\":{\"label\":\"Provider_metadata\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Provider_metadata\",\"searchable\":false,\"sortable\":false}},\"related\":{\"edit\":{\"label\":\"Related\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"id\"},\"list\":{\"label\":\"Related\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"hash\",\"sha256\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"hash\",\"size\":6}],[{\"name\":\"sha256\",\"size\":6},{\"name\":\"ext\",\"size\":6}],[{\"name\":\"mime\",\"size\":6},{\"name\":\"size\",\"size\":6}],[{\"name\":\"url\",\"size\":6},{\"name\":\"provider\",\"size\":6}],[{\"name\":\"provider_metadata\",\"size\":12}]],\"editRelations\":[\"related\"]}}','object','',''),
	(16,'plugin_content_manager_configuration_content_types::users-permissions.user','{\"uid\":\"user\",\"source\":\"users-permissions\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"username\",\"defaultSortBy\":\"username\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"username\":{\"edit\":{\"label\":\"Username\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Username\",\"searchable\":true,\"sortable\":true}},\"email\":{\"edit\":{\"label\":\"Email\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Email\",\"searchable\":true,\"sortable\":true}},\"provider\":{\"edit\":{\"label\":\"Provider\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Provider\",\"searchable\":true,\"sortable\":true}},\"password\":{\"edit\":{\"label\":\"Password\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Password\",\"searchable\":true,\"sortable\":true}},\"resetPasswordToken\":{\"edit\":{\"label\":\"ResetPasswordToken\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"ResetPasswordToken\",\"searchable\":true,\"sortable\":true}},\"confirmed\":{\"edit\":{\"label\":\"Confirmed\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Confirmed\",\"searchable\":true,\"sortable\":true}},\"blocked\":{\"edit\":{\"label\":\"Blocked\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Blocked\",\"searchable\":true,\"sortable\":true}},\"role\":{\"edit\":{\"label\":\"Role\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Role\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"username\",\"email\",\"provider\"],\"edit\":[[{\"name\":\"username\",\"size\":6},{\"name\":\"email\",\"size\":6}],[{\"name\":\"provider\",\"size\":6},{\"name\":\"password\",\"size\":6}],[{\"name\":\"resetPasswordToken\",\"size\":6},{\"name\":\"confirmed\",\"size\":4}],[{\"name\":\"blocked\",\"size\":4}]],\"editRelations\":[\"role\"]}}','object','',''),
	(17,'plugin_users-permissions_advanced','{\"unique_email\":true,\"allow_register\":true,\"email_confirmation\":false,\"email_confirmation_redirection\":\"http://localhost:1337/admin\",\"email_reset_password\":\"http://localhost:1337/admin\",\"default_role\":\"authenticated\"}','object','',''),
	(18,'db_model_categories','{\"name\":{\"unique\":true,\"required\":true,\"type\":\"string\"},\"remark\":{\"type\":\"text\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(19,'plugin_content_manager_configuration_content_types::category','{\"uid\":\"category\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"remark\",\"created_at\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"remark\",\"size\":6}]],\"editRelations\":[]}}','object','',''),
	(20,'db_model_contacts','{\"type\":{\"enum\":[\"FACEBOOK\",\"INSTAGRAM\",\"TEL\",\"OPENRICE\",\"TELEGRAM\",\"OTHER\"],\"type\":\"enumeration\"},\"context\":{\"type\":\"text\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(22,'db_model_districts','{\"name\":{\"unique\":true,\"required\":true,\"type\":\"string\"},\"remark\":{\"type\":\"text\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(23,'plugin_content_manager_configuration_content_types::district','{\"uid\":\"district\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"remark\",\"created_at\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"remark\",\"size\":6}]],\"editRelations\":[]}}','object','',''),
	(24,'db_model_fcs','{\"source\":{\"required\":true,\"type\":\"text\"},\"reason\":{\"required\":true,\"type\":\"text\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(26,'db_model_franchisegroups','{\"name\":{\"required\":true,\"unique\":true,\"type\":\"string\"},\"hqAddress\":{\"type\":\"text\"},\"remark\":{\"type\":\"text\"},\"ybgType\":{\"enum\":[\"YELLOW\",\"BLUE\",\"GREEN\",\"UNKNOWN\"],\"type\":\"enumeration\"},\"merchats\":{\"collection\":\"merchat\",\"attribute\":\"merchat\",\"column\":\"id\",\"isVirtual\":true},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(27,'plugin_content_manager_configuration_content_types::franchisegroup','{\"uid\":\"franchisegroup\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"hqAddress\":{\"edit\":{\"label\":\"HqAddress\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"HqAddress\",\"searchable\":true,\"sortable\":true}},\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"ybgType\":{\"edit\":{\"label\":\"YbgType\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"YbgType\",\"searchable\":true,\"sortable\":true}},\"merchats\":{\"edit\":{\"label\":\"Merchats\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Merchats\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"hqAddress\",\"remark\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"hqAddress\",\"size\":6}],[{\"name\":\"remark\",\"size\":6},{\"name\":\"ybgType\",\"size\":6}]],\"editRelations\":[\"merchats\"]}}','object','',''),
	(28,'db_model_merchats','{\"name\":{\"unique\":true,\"required\":true,\"type\":\"string\"},\"remark\":{\"type\":\"text\"},\"ybgType\":{\"enum\":[\"YELLOW\",\"BLUE\",\"GREEN\",\"UNKNOWN\"],\"type\":\"enumeration\"},\"shopes\":{\"collection\":\"shop\",\"via\":\"merchat\",\"isVirtual\":true},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(29,'plugin_content_manager_configuration_content_types::merchat','{\"uid\":\"merchat\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"ybgType\":{\"edit\":{\"label\":\"YbgType\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"YbgType\",\"searchable\":true,\"sortable\":true}},\"shopes\":{\"edit\":{\"label\":\"Shopes\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Shopes\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"remark\",\"ybgType\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"remark\",\"size\":6}],[{\"name\":\"ybgType\",\"size\":6}]],\"editRelations\":[\"shopes\"]}}','object','',''),
	(30,'db_model_pushers','{\"name\":{\"unique\":true,\"required\":true,\"type\":\"string\"},\"contact\":{\"type\":\"text\"},\"remark\":{\"type\":\"text\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(31,'plugin_content_manager_configuration_content_types::pusher','{\"uid\":\"pusher\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"contact\":{\"edit\":{\"label\":\"Contact\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Contact\",\"searchable\":true,\"sortable\":true}},\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"contact\",\"remark\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"contact\",\"size\":6}],[{\"name\":\"remark\",\"size\":6}]],\"editRelations\":[]}}','object','',''),
	(32,'db_model_shops','{\"name\":{\"type\":\"string\",\"unique\":true,\"required\":true},\"lat\":{\"type\":\"float\"},\"long\":{\"type\":\"float\"},\"address\":{\"type\":\"text\"},\"district\":{\"model\":\"district\"},\"category\":{\"model\":\"category\"},\"merchat\":{\"model\":\"merchat\",\"via\":\"shopes\"},\"openHours\":{\"type\":\"text\"},\"status\":{\"enum\":[\"NORMAL\",\"PERMANENT_CLOSED\",\"DELISTING\"],\"type\":\"enumeration\"},\"ybgType\":{\"enum\":[\"YELLOW\",\"BLUE\",\"GREEN\",\"UNKNOWN\"],\"type\":\"enumeration\"},\"remark\":{\"type\":\"text\"},\"isHidden\":{\"type\":\"boolean\",\"required\":true},\"comment\":{\"type\":\"text\"},\"contacts\":{\"group\":\"contact\",\"repeatable\":true,\"type\":\"group\"},\"fcs\":{\"group\":\"fc\",\"repeatable\":true,\"type\":\"group\"},\"entries\":{\"collection\":\"entry\",\"via\":\"shop\",\"isVirtual\":true},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(33,'plugin_content_manager_configuration_content_types::shop','{\"uid\":\"shop\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"lat\":{\"edit\":{\"label\":\"Lat\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Lat\",\"searchable\":true,\"sortable\":true}},\"long\":{\"edit\":{\"label\":\"Long\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Long\",\"searchable\":true,\"sortable\":true}},\"address\":{\"edit\":{\"label\":\"Address\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Address\",\"searchable\":true,\"sortable\":true}},\"district\":{\"edit\":{\"label\":\"District\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"District\",\"searchable\":false,\"sortable\":false}},\"category\":{\"edit\":{\"label\":\"Category\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Category\",\"searchable\":false,\"sortable\":false}},\"merchat\":{\"edit\":{\"label\":\"Merchat\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Merchat\",\"searchable\":false,\"sortable\":false}},\"openHours\":{\"edit\":{\"label\":\"OpenHours\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"OpenHours\",\"searchable\":true,\"sortable\":true}},\"status\":{\"edit\":{\"label\":\"Status\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Status\",\"searchable\":true,\"sortable\":true}},\"ybgType\":{\"edit\":{\"label\":\"YbgType\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"YbgType\",\"searchable\":true,\"sortable\":true}},\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"isHidden\":{\"edit\":{\"label\":\"IsHidden\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"IsHidden\",\"searchable\":true,\"sortable\":true}},\"comment\":{\"edit\":{\"label\":\"Comment\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Comment\",\"searchable\":true,\"sortable\":true}},\"contacts\":{\"edit\":{\"label\":\"Contacts\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Contacts\",\"searchable\":false,\"sortable\":false}},\"fcs\":{\"edit\":{\"label\":\"Fcs\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Fcs\",\"searchable\":false,\"sortable\":false}},\"entries\":{\"edit\":{\"label\":\"Entries\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Entries\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"lat\",\"long\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"lat\",\"size\":4}],[{\"name\":\"long\",\"size\":4},{\"name\":\"address\",\"size\":6}],[{\"name\":\"openHours\",\"size\":6},{\"name\":\"status\",\"size\":6}],[{\"name\":\"ybgType\",\"size\":6},{\"name\":\"remark\",\"size\":6}],[{\"name\":\"isHidden\",\"size\":4},{\"name\":\"comment\",\"size\":6}],[{\"name\":\"contacts\",\"size\":12}],[{\"name\":\"fcs\",\"size\":12}]],\"editRelations\":[\"district\",\"category\",\"merchat\",\"entries\"]}}','object','',''),
	(34,'db_model_pushes','{\"name\":{\"unique\":true,\"required\":true,\"type\":\"string\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(36,'db_model_shops__pushes','{\"shop_id\":{\"type\":\"integer\"},\"push_id\":{\"type\":\"integer\"}}','object',NULL,NULL),
	(37,'db_model_pushes__fcs','{\"push_id\":{\"type\":\"integer\"},\"fc_id\":{\"type\":\"integer\"}}','object',NULL,NULL),
	(43,'db_model_entries','{\"name\":{\"required\":true,\"unique\":true,\"type\":\"string\"},\"pusher\":{\"model\":\"pusher\"},\"lat\":{\"type\":\"float\"},\"long\":{\"type\":\"float\"},\"address\":{\"type\":\"text\"},\"district\":{\"model\":\"district\"},\"category\":{\"model\":\"category\"},\"merchat\":{\"model\":\"merchat\"},\"openHours\":{\"type\":\"json\"},\"status\":{\"enum\":[\"NORMAL\",\"PERMANENT_CLOSED\",\"DELISTING\"],\"type\":\"enumeration\"},\"ybgType\":{\"enum\":[\"YELLOW\",\"BLUE\",\"GREEN\",\"UNKNOWN\"],\"type\":\"enumeration\"},\"remark\":{\"type\":\"text\"},\"isHidden\":{\"required\":true,\"type\":\"boolean\"},\"comment\":{\"type\":\"text\"},\"shop\":{\"model\":\"shop\",\"via\":\"entries\"},\"contacts\":{\"group\":\"contact\",\"repeatable\":true,\"type\":\"group\"},\"fcs\":{\"group\":\"fc\",\"repeatable\":true,\"type\":\"group\"},\"created_at\":{\"type\":\"timestamp\"},\"updated_at\":{\"type\":\"timestampUpdate\"}}','object',NULL,NULL),
	(44,'plugin_content_manager_configuration_content_types::entry','{\"uid\":\"entry\",\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"name\",\"defaultSortBy\":\"name\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"remark\":{\"edit\":{\"label\":\"Remark\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Remark\",\"searchable\":true,\"sortable\":true}},\"openHours\":{\"edit\":{\"label\":\"OpenHours\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"OpenHours\",\"searchable\":false,\"sortable\":false}},\"pusher\":{\"edit\":{\"label\":\"Pusher\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Pusher\",\"searchable\":false,\"sortable\":false}},\"created_at\":{\"edit\":{\"label\":\"Created_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Created_at\",\"searchable\":true,\"sortable\":true}},\"name\":{\"edit\":{\"label\":\"Name\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Name\",\"searchable\":true,\"sortable\":true}},\"district\":{\"edit\":{\"label\":\"District\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"District\",\"searchable\":false,\"sortable\":false}},\"merchat\":{\"edit\":{\"label\":\"Merchat\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Merchat\",\"searchable\":false,\"sortable\":false}},\"status\":{\"edit\":{\"label\":\"Status\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Status\",\"searchable\":true,\"sortable\":true}},\"contacts\":{\"edit\":{\"label\":\"Contacts\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Contacts\",\"searchable\":false,\"sortable\":false}},\"isHidden\":{\"edit\":{\"label\":\"IsHidden\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"IsHidden\",\"searchable\":true,\"sortable\":true}},\"address\":{\"edit\":{\"label\":\"Address\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Address\",\"searchable\":true,\"sortable\":true}},\"shop\":{\"edit\":{\"label\":\"Shop\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Shop\",\"searchable\":false,\"sortable\":false}},\"fcs\":{\"edit\":{\"label\":\"Fcs\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Fcs\",\"searchable\":false,\"sortable\":false}},\"updated_at\":{\"edit\":{\"label\":\"Updated_at\",\"description\":\"\",\"placeholder\":\"\",\"visible\":false,\"editable\":true},\"list\":{\"label\":\"Updated_at\",\"searchable\":true,\"sortable\":true}},\"ybgType\":{\"edit\":{\"label\":\"YbgType\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"YbgType\",\"searchable\":true,\"sortable\":true}},\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":true,\"sortable\":true}},\"long\":{\"edit\":{\"label\":\"Long\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Long\",\"searchable\":true,\"sortable\":true}},\"category\":{\"edit\":{\"label\":\"Category\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Category\",\"searchable\":false,\"sortable\":false}},\"lat\":{\"edit\":{\"label\":\"Lat\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Lat\",\"searchable\":true,\"sortable\":true}},\"comment\":{\"edit\":{\"label\":\"Comment\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Comment\",\"searchable\":true,\"sortable\":true}}},\"layouts\":{\"list\":[\"id\",\"name\",\"status\"],\"edit\":[[{\"name\":\"name\",\"size\":6},{\"name\":\"lat\",\"size\":4}],[{\"name\":\"long\",\"size\":4},{\"name\":\"address\",\"size\":6}],[{\"name\":\"openHours\",\"size\":12}],[{\"name\":\"status\",\"size\":6},{\"name\":\"ybgType\",\"size\":6}],[{\"name\":\"remark\",\"size\":6},{\"name\":\"isHidden\",\"size\":4}],[{\"name\":\"comment\",\"size\":6}],[{\"name\":\"contacts\",\"size\":12}],[{\"name\":\"fcs\",\"size\":12}]],\"editRelations\":[\"shop\",\"pusher\",\"district\",\"category\",\"merchat\"]}}','object','',''),
	(45,'db_model_entries__fcs','{\"entry_id\":{\"type\":\"integer\"},\"fc_id\":{\"type\":\"integer\"}}','object',NULL,NULL),
	(46,'db_model_franchisegroups__merchats','{\"franchisegroup_id\":{\"type\":\"integer\"},\"merchat_id\":{\"type\":\"integer\"}}','object',NULL,NULL),
	(47,'db_model_groups_contacts','{\"type\":{\"enum\":[\"FACEBOOK\",\"INSTAGRAM\",\"TEL\",\"OPENRICE\",\"TELEGRAM\",\"OTHER\"],\"type\":\"enumeration\"},\"context\":{\"required\":true,\"type\":\"text\"},\"pusher\":{\"model\":\"pusher\"}}','object',NULL,NULL),
	(48,'plugin_content_manager_configuration_groups::contact','{\"uid\":\"contact\",\"isGroup\":true,\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"id\",\"defaultSortBy\":\"id\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"type\":{\"edit\":{\"label\":\"Type\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Type\",\"searchable\":true,\"sortable\":true}},\"context\":{\"edit\":{\"label\":\"Context\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Context\",\"searchable\":true,\"sortable\":true}},\"pusher\":{\"edit\":{\"label\":\"Pusher\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Pusher\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"type\",\"context\"],\"edit\":[[{\"name\":\"type\",\"size\":6},{\"name\":\"context\",\"size\":6}],[{\"name\":\"pusher\",\"size\":6}]],\"editRelations\":[]}}','object','',''),
	(49,'db_model_groups_fcs','{\"source\":{\"required\":true,\"type\":\"text\"},\"context\":{\"unique\":false,\"required\":true,\"type\":\"text\"},\"pusher\":{\"model\":\"pusher\"}}','object',NULL,NULL),
	(50,'plugin_content_manager_configuration_groups::fc','{\"uid\":\"fc\",\"isGroup\":true,\"settings\":{\"searchable\":true,\"filterable\":true,\"bulkable\":true,\"pageSize\":10,\"mainField\":\"id\",\"defaultSortBy\":\"id\",\"defaultSortOrder\":\"ASC\"},\"metadatas\":{\"id\":{\"edit\":{},\"list\":{\"label\":\"Id\",\"searchable\":false,\"sortable\":false}},\"source\":{\"edit\":{\"label\":\"Source\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Source\",\"searchable\":true,\"sortable\":true}},\"context\":{\"edit\":{\"label\":\"Context\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true},\"list\":{\"label\":\"Context\",\"searchable\":true,\"sortable\":true}},\"pusher\":{\"edit\":{\"label\":\"Pusher\",\"description\":\"\",\"placeholder\":\"\",\"visible\":true,\"editable\":true,\"mainField\":\"name\"},\"list\":{\"label\":\"Pusher\",\"searchable\":false,\"sortable\":false}}},\"layouts\":{\"list\":[\"id\",\"source\",\"context\"],\"edit\":[[{\"name\":\"source\",\"size\":6},{\"name\":\"context\",\"size\":6}],[{\"name\":\"pusher\",\"size\":6}]],\"editRelations\":[]}}','object','','');

/*!40000 ALTER TABLE `core_store` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table districts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `districts`;

CREATE TABLE `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `remark` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `districts_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_DISTRICTS` (`name`,`remark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entries`;

CREATE TABLE `entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `shop` int(11) DEFAULT NULL,
  `pusher` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `long` double DEFAULT NULL,
  `address` longtext DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `merchat` int(11) DEFAULT NULL,
  `openHours` longtext DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `ybgType` varchar(255) DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `isHidden` tinyint(1) NOT NULL,
  `comment` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entries_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_ENTRIES` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table entries__fcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entries__fcs`;

CREATE TABLE `entries__fcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) DEFAULT NULL,
  `fc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table entries_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entries_groups`;

CREATE TABLE `entries_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `group_type` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entries_groups_entry_id_foreign` (`entry_id`),
  CONSTRAINT `entries_groups_entry_id_foreign` FOREIGN KEY (`entry_id`) REFERENCES `entries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table fcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fcs`;

CREATE TABLE `fcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` longtext NOT NULL,
  `reason` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `push` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_FCS` (`source`,`reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table franchisegroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `franchisegroups`;

CREATE TABLE `franchisegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `hqAddress` longtext DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `ybgType` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `franchisegroups_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_FRANCHISEGROUPS` (`name`,`hqAddress`,`remark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table franchisegroups__merchats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `franchisegroups__merchats`;

CREATE TABLE `franchisegroups__merchats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `franchisegroup_id` int(11) DEFAULT NULL,
  `merchat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table groups_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_contacts`;

CREATE TABLE `groups_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `context` longtext NOT NULL,
  `pusher` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_GROUPS_CONTACTS` (`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table groups_fcs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_fcs`;

CREATE TABLE `groups_fcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` longtext NOT NULL,
  `context` longtext NOT NULL,
  `pusher` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_GROUPS_FCS` (`source`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table merchats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `merchats`;

CREATE TABLE `merchats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `remark` longtext DEFAULT NULL,
  `ybgType` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `shopes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `merchats_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_MERCHATS` (`name`,`remark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table pushers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pushers`;

CREATE TABLE `pushers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `contact` longtext DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `pushers_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_PUSHERS` (`name`,`contact`,`remark`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table shops
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops`;

CREATE TABLE `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lat` double DEFAULT NULL,
  `long` double DEFAULT NULL,
  `address` longtext DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `merchat` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `openHours` longtext DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `ybgType` varchar(255) DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `isHidden` tinyint(1) NOT NULL,
  `comment` longtext DEFAULT NULL,
  `pushes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shops_name_unique` (`name`),
  FULLTEXT KEY `SEARCH_SHOPS` (`name`,`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table shops_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shops_groups`;

CREATE TABLE `shops_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `group_type` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shops_groups_shop_id_foreign` (`shop_id`),
  CONSTRAINT `shops_groups_shop_id_foreign` FOREIGN KEY (`shop_id`) REFERENCES `shops` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table upload_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `upload_file`;

CREATE TABLE `upload_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `sha256` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `mime` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `provider_metadata` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_UPLOAD_FILE` (`name`,`hash`,`sha256`,`ext`,`mime`,`size`,`url`,`provider`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table upload_file_morph
# ------------------------------------------------------------

DROP TABLE IF EXISTS `upload_file_morph`;

CREATE TABLE `upload_file_morph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_file_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  `related_type` longtext DEFAULT NULL,
  `field` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_UPLOAD_FILE_MORPH` (`related_type`,`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table users-permissions_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users-permissions_permission`;

CREATE TABLE `users-permissions_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `policy` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `SEARCH_USERS_PERMISSIONS_PERMISSION` (`type`,`controller`,`action`,`policy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `users-permissions_permission` WRITE;
/*!40000 ALTER TABLE `users-permissions_permission` DISABLE KEYS */;

INSERT INTO `users-permissions_permission` (`id`, `type`, `controller`, `action`, `enabled`, `policy`, `role`)
VALUES
	(1,'content-type-builder','contenttypebuilder','getmodels',0,'',1),
	(2,'content-type-builder','contenttypebuilder','getmodel',0,'',1),
	(3,'content-type-builder','contenttypebuilder','getconnections',0,'',1),
	(4,'content-type-builder','contenttypebuilder','createmodel',0,'',1),
	(5,'content-type-builder','contenttypebuilder','updatemodel',0,'',1),
	(6,'content-type-builder','contenttypebuilder','deletemodel',0,'',1),
	(7,'content-type-builder','groups','getgroups',0,'',1),
	(8,'content-type-builder','groups','getgroup',0,'',1),
	(9,'content-type-builder','groups','creategroup',0,'',1),
	(10,'content-type-builder','groups','updategroup',0,'',1),
	(11,'content-type-builder','groups','deletegroup',0,'',1),
	(12,'content-manager','contentmanager','find',0,'',1),
	(13,'content-manager','contentmanager','findone',0,'',1),
	(14,'content-manager','contentmanager','count',0,'',1),
	(15,'content-manager','contentmanager','create',0,'',1),
	(16,'content-manager','contentmanager','update',0,'',1),
	(17,'content-manager','contentmanager','delete',0,'',1),
	(18,'content-manager','contentmanager','deletemany',0,'',1),
	(19,'content-manager','contenttypes','listcontenttypes',0,'',1),
	(20,'content-manager','contenttypes','findcontenttype',0,'',1),
	(21,'content-manager','contenttypes','updatecontenttype',0,'',1),
	(22,'content-manager','generalsettings','getgeneralsettings',0,'',1),
	(23,'content-manager','generalsettings','updategeneralsettings',0,'',1),
	(24,'content-manager','groups','listgroups',0,'',1),
	(25,'content-manager','groups','findgroup',0,'',1),
	(26,'content-manager','groups','updategroup',0,'',1),
	(27,'users-permissions','auth','callback',0,'',1),
	(28,'users-permissions','auth','changepassword',0,'',1),
	(29,'users-permissions','auth','connect',1,'',1),
	(30,'users-permissions','auth','forgotpassword',0,'',1),
	(31,'users-permissions','auth','register',0,'',1),
	(32,'users-permissions','auth','emailconfirmation',0,'',1),
	(33,'users-permissions','auth','sendemailconfirmation',0,'',1),
	(34,'users-permissions','user','find',0,'',1),
	(35,'users-permissions','user','me',1,'',1),
	(36,'users-permissions','user','findone',0,'',1),
	(37,'users-permissions','user','create',0,'',1),
	(38,'users-permissions','user','update',0,'',1),
	(39,'users-permissions','user','destroy',0,'',1),
	(40,'users-permissions','user','destroyall',0,'',1),
	(41,'users-permissions','userspermissions','createrole',0,'',1),
	(42,'users-permissions','userspermissions','deleteprovider',0,'',1),
	(43,'users-permissions','userspermissions','deleterole',0,'',1),
	(44,'users-permissions','userspermissions','getpermissions',0,'',1),
	(45,'users-permissions','userspermissions','getpolicies',0,'',1),
	(46,'users-permissions','userspermissions','getrole',0,'',1),
	(47,'users-permissions','userspermissions','getroles',0,'',1),
	(48,'users-permissions','userspermissions','getroutes',0,'',1),
	(49,'users-permissions','userspermissions','index',0,'',1),
	(50,'users-permissions','userspermissions','init',1,'',1),
	(51,'users-permissions','userspermissions','searchusers',0,'',1),
	(52,'users-permissions','userspermissions','updaterole',0,'',1),
	(53,'users-permissions','userspermissions','getemailtemplate',0,'',1),
	(54,'users-permissions','userspermissions','updateemailtemplate',0,'',1),
	(55,'users-permissions','userspermissions','getadvancedsettings',0,'',1),
	(56,'users-permissions','userspermissions','updateadvancedsettings',0,'',1),
	(57,'users-permissions','userspermissions','getproviders',0,'',1),
	(58,'users-permissions','userspermissions','updateproviders',0,'',1),
	(59,'email','email','send',0,'',1),
	(60,'email','email','getenvironments',0,'',1),
	(61,'email','email','getsettings',0,'',1),
	(62,'email','email','updatesettings',0,'',1),
	(63,'upload','upload','upload',0,'',1),
	(64,'upload','upload','getenvironments',0,'',1),
	(65,'upload','upload','getsettings',0,'',1),
	(66,'upload','upload','updatesettings',0,'',1),
	(67,'upload','upload','find',0,'',1),
	(68,'upload','upload','findone',0,'',1),
	(69,'upload','upload','count',0,'',1),
	(70,'upload','upload','destroy',0,'',1),
	(71,'upload','upload','search',0,'',1),
	(72,'content-type-builder','contenttypebuilder','getmodels',0,'',2),
	(73,'content-type-builder','contenttypebuilder','getmodel',0,'',2),
	(74,'content-type-builder','contenttypebuilder','getconnections',0,'',2),
	(75,'content-type-builder','contenttypebuilder','createmodel',0,'',2),
	(76,'content-type-builder','contenttypebuilder','updatemodel',0,'',2),
	(77,'content-type-builder','contenttypebuilder','deletemodel',0,'',2),
	(78,'content-type-builder','groups','getgroups',0,'',2),
	(79,'content-type-builder','groups','getgroup',0,'',2),
	(80,'content-type-builder','groups','creategroup',0,'',2),
	(81,'content-type-builder','groups','updategroup',0,'',2),
	(82,'content-type-builder','groups','deletegroup',0,'',2),
	(83,'content-manager','contentmanager','find',0,'',2),
	(84,'content-manager','contentmanager','findone',0,'',2),
	(85,'content-manager','contentmanager','count',0,'',2),
	(86,'content-manager','contentmanager','create',0,'',2),
	(87,'content-manager','contentmanager','update',0,'',2),
	(88,'content-manager','contentmanager','delete',0,'',2),
	(89,'content-manager','contentmanager','deletemany',0,'',2),
	(90,'content-manager','contenttypes','listcontenttypes',0,'',2),
	(91,'content-manager','contenttypes','findcontenttype',0,'',2),
	(92,'content-manager','contenttypes','updatecontenttype',0,'',2),
	(93,'content-manager','generalsettings','getgeneralsettings',0,'',2),
	(94,'content-manager','generalsettings','updategeneralsettings',0,'',2),
	(95,'content-manager','groups','listgroups',0,'',2),
	(96,'content-manager','groups','findgroup',0,'',2),
	(97,'content-manager','groups','updategroup',0,'',2),
	(98,'users-permissions','auth','callback',1,'',2),
	(99,'users-permissions','auth','changepassword',1,'',2),
	(100,'users-permissions','auth','connect',1,'',2),
	(101,'users-permissions','auth','forgotpassword',1,'',2),
	(102,'users-permissions','auth','register',1,'',2),
	(103,'users-permissions','auth','emailconfirmation',1,'',2),
	(104,'users-permissions','auth','sendemailconfirmation',0,'',2),
	(105,'users-permissions','user','find',0,'',2),
	(106,'users-permissions','user','me',1,'',2),
	(107,'users-permissions','user','findone',0,'',2),
	(108,'users-permissions','user','create',0,'',2),
	(109,'users-permissions','user','update',0,'',2),
	(110,'users-permissions','user','destroy',0,'',2),
	(111,'users-permissions','user','destroyall',0,'',2),
	(112,'users-permissions','userspermissions','createrole',0,'',2),
	(113,'users-permissions','userspermissions','deleteprovider',0,'',2),
	(114,'users-permissions','userspermissions','deleterole',0,'',2),
	(115,'users-permissions','userspermissions','getpermissions',0,'',2),
	(116,'users-permissions','userspermissions','getpolicies',0,'',2),
	(117,'users-permissions','userspermissions','getrole',0,'',2),
	(118,'users-permissions','userspermissions','getroles',0,'',2),
	(119,'users-permissions','userspermissions','getroutes',0,'',2),
	(120,'users-permissions','userspermissions','index',0,'',2),
	(121,'users-permissions','userspermissions','init',1,'',2),
	(122,'users-permissions','userspermissions','searchusers',0,'',2),
	(123,'users-permissions','userspermissions','updaterole',0,'',2),
	(124,'users-permissions','userspermissions','getemailtemplate',0,'',2),
	(125,'users-permissions','userspermissions','updateemailtemplate',0,'',2),
	(126,'users-permissions','userspermissions','getadvancedsettings',0,'',2),
	(127,'users-permissions','userspermissions','updateadvancedsettings',0,'',2),
	(128,'users-permissions','userspermissions','getproviders',0,'',2),
	(129,'users-permissions','userspermissions','updateproviders',0,'',2),
	(130,'email','email','send',0,'',2),
	(131,'email','email','getenvironments',0,'',2),
	(132,'email','email','getsettings',0,'',2),
	(133,'email','email','updatesettings',0,'',2),
	(134,'upload','upload','upload',0,'',2),
	(135,'upload','upload','getenvironments',0,'',2),
	(136,'upload','upload','getsettings',0,'',2),
	(137,'upload','upload','updatesettings',0,'',2),
	(138,'upload','upload','find',0,'',2),
	(139,'upload','upload','findone',0,'',2),
	(140,'upload','upload','count',0,'',2),
	(141,'upload','upload','destroy',0,'',2),
	(142,'upload','upload','search',0,'',2),
	(143,'application','category','find',0,'',1),
	(144,'application','category','findone',0,'',1),
	(145,'application','category','count',0,'',1),
	(146,'application','category','create',0,'',1),
	(147,'application','category','update',0,'',1),
	(148,'application','category','delete',0,'',1),
	(149,'application','category','find',0,'',2),
	(150,'application','category','findone',0,'',2),
	(151,'application','category','count',0,'',2),
	(152,'application','category','create',0,'',2),
	(153,'application','category','update',0,'',2),
	(154,'application','category','delete',0,'',2),
	(167,'application','district','find',0,'',1),
	(168,'application','district','findone',0,'',1),
	(169,'application','district','create',0,'',1),
	(170,'application','district','count',0,'',1),
	(171,'application','district','update',0,'',1),
	(172,'application','district','delete',0,'',1),
	(173,'application','district','find',0,'',2),
	(174,'application','district','findone',0,'',2),
	(175,'application','district','count',0,'',2),
	(176,'application','district','create',0,'',2),
	(177,'application','district','update',0,'',2),
	(178,'application','district','delete',0,'',2),
	(191,'application','franchisegroup','find',0,'',1),
	(192,'application','franchisegroup','findone',0,'',1),
	(193,'application','franchisegroup','count',0,'',1),
	(194,'application','franchisegroup','create',0,'',1),
	(195,'application','franchisegroup','update',0,'',1),
	(196,'application','franchisegroup','delete',0,'',1),
	(197,'application','franchisegroup','find',0,'',2),
	(198,'application','franchisegroup','findone',0,'',2),
	(199,'application','franchisegroup','count',0,'',2),
	(200,'application','franchisegroup','create',0,'',2),
	(201,'application','franchisegroup','update',0,'',2),
	(202,'application','franchisegroup','delete',0,'',2),
	(203,'application','merchat','find',0,'',1),
	(204,'application','merchat','findone',0,'',1),
	(205,'application','merchat','count',0,'',1),
	(206,'application','merchat','create',0,'',1),
	(207,'application','merchat','update',0,'',1),
	(208,'application','merchat','delete',0,'',1),
	(209,'application','merchat','find',0,'',2),
	(210,'application','merchat','findone',0,'',2),
	(211,'application','merchat','count',0,'',2),
	(212,'application','merchat','create',0,'',2),
	(213,'application','merchat','update',0,'',2),
	(214,'application','merchat','delete',0,'',2),
	(215,'application','pusher','find',0,'',1),
	(216,'application','pusher','findone',0,'',1),
	(217,'application','pusher','count',0,'',1),
	(218,'application','pusher','create',0,'',1),
	(219,'application','pusher','update',0,'',1),
	(220,'application','pusher','delete',0,'',1),
	(221,'application','pusher','find',0,'',2),
	(222,'application','pusher','findone',0,'',2),
	(223,'application','pusher','count',0,'',2),
	(224,'application','pusher','create',0,'',2),
	(225,'application','pusher','update',0,'',2),
	(226,'application','pusher','delete',0,'',2),
	(227,'application','shop','find',0,'',1),
	(228,'application','shop','findone',0,'',1),
	(229,'application','shop','count',0,'',1),
	(230,'application','shop','create',0,'',1),
	(231,'application','shop','update',0,'',1),
	(232,'application','shop','delete',0,'',1),
	(233,'application','shop','find',0,'',2),
	(234,'application','shop','findone',0,'',2),
	(235,'application','shop','count',0,'',2),
	(236,'application','shop','create',0,'',2),
	(237,'application','shop','update',0,'',2),
	(238,'application','shop','delete',0,'',2),
	(311,'application','entry','find',0,'',1),
	(312,'application','entry','findone',0,'',1),
	(313,'application','entry','count',0,'',1),
	(314,'application','entry','create',0,'',1),
	(315,'application','entry','update',0,'',1),
	(316,'application','entry','delete',0,'',1),
	(317,'application','entry','find',0,'',2),
	(318,'application','entry','findone',0,'',2),
	(319,'application','entry','count',0,'',2),
	(320,'application','entry','create',0,'',2),
	(321,'application','entry','update',0,'',2),
	(322,'application','entry','delete',0,'',2);

/*!40000 ALTER TABLE `users-permissions_permission` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users-permissions_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users-permissions_role`;

CREATE TABLE `users-permissions_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users-permissions_role_type_unique` (`type`),
  FULLTEXT KEY `SEARCH_USERS_PERMISSIONS_ROLE` (`name`,`description`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `users-permissions_role` WRITE;
/*!40000 ALTER TABLE `users-permissions_role` DISABLE KEYS */;

INSERT INTO `users-permissions_role` (`id`, `name`, `description`, `type`)
VALUES
	(1,'Authenticated','Default role given to authenticated user.','authenticated'),
	(2,'Public','Default role given to unauthenticated user.','public');

/*!40000 ALTER TABLE `users-permissions_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users-permissions_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users-permissions_user`;

CREATE TABLE `users-permissions_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `resetPasswordToken` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `blocked` tinyint(1) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `users-permissions_user_username_unique` (`username`),
  FULLTEXT KEY `SEARCH_USERS_PERMISSIONS_USER` (`username`,`provider`,`resetPasswordToken`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
